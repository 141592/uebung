package bowling;

public class Player {
	protected String name; 
	protected int id; 
	
	Player(String name, int id) {
		this.name = name; 
		this.id = id; 	
	}

	public String getName() {
		return this.name; 	
	}

	public int getID() {
		return this.id; 	
	}

}
