package bowling; 
import java.util.*;
import bowling.IGame;

/**
 * Implements IGame interface
 * 
 * @authors Davide Toldo, Felix Wutschke
 */
public abstract class Game implements IGame {
	
	private int max_players, current_player, player_count; 
	private int pins_hit, pins_standing; 	
	private boolean game_is_running;  
	private List<Player> players; 
	private String game_mode; // bowling or tannenbaum

	public Game(int max_player) {
		this.max_players = max_player; 
	}	
	
    /**
     * Adds a player to the game
     * 
     * @param name the name of the player
     * @return the player object, or null if an error occurred
     */
	@Override
    public Player addPlayer(String name) {
		Player pl = new Player(name, players.size()); 
		return pl; 
	}
    
    /**
     * Returns the current player
     * 
     * @return the current player
     */
	@Override
    public Player getActivePlayer() {
		return players.get(current_player); 
	}
    
    /**
     * Returns the actual number of active players
     * 
     * @return the actual number of active players
     */
	@Override
    public int getActivePlayerCount() {
		return players.size(); 
	}	
    
    /**
     * Returns the maximum number of active players
     * 
     * @return the maximum number of active players
     */
	@Override
    public int getMaxPlayerCount() {
		return max_players;
	}
    
    /**
     * Returns the name of this game mode
     * 
     * @return the name of the game mode
     */
	@Override
	public String getName() {
		return game_mode; 
	}

    /**
     * Returns the number of pins in this game mode
     * 
     * @return the number of pins
     */
	@Override
    public int getPinCount() {
	
	}

    /**
     * Returns the number of pins currently standing 
     * 
     * @return the number of pins still standing
     */
	@Override
	public int getPinsLeft();

    /**
     * Returns the player with the given ID, or null if there is no such player
     *
     * @param id the ID of the player we are looking for
     * @return the player with ID id or null if there is not such player
     */
	@Override
	public Player getPlayer(int id);	 

    /**
     * Returns the current round
     * 
     * @return the round, starting with 1
     */
	@Override
	public int getRound();
    
    /**	
     * Returns the maximum number of rounds
     * 
     * @return the maximum number of rounds
     */
	@Override
	public int getRoundCount();
    
    /**
     * Returns the score for a given player. The format depends on the game mode.
     * 
     * @param player the given player
     * @return the score of the selected player
     */
	@Override
	public int[] getScore(Player player);

    /**
     * Returns the current throw number
     * 
     * @return the throw number, starting with 1
     */
	@Override
	public int getThrow();

    /**
     * Returns the player who has won the game.
     * 
     * @return player who won, or null if an error occurred
     */
	@Override
	public Player getWinner();

    /**
     * Indicates if the game has been finished
     * 
     * @return true if the game is complete
     */
	@Override
	public boolean hasFinished();

    /**
     * Indicates if the game has been started
     * 
     * @return true if the game is running
     */
	@Override
	public boolean hasStarted();

    /**
     * Starts the game
     * 
     * @return true if the game was started, false if any error occurred, e.g., not
     *         enough players
     */
	@Override
	public boolean startGame();
    
    /**
     * Throws a ball
     * 
     * @param count
     * @return true if the throw was valid, false if any error occurred, e.g., game
     *         not started, invalid count, ...
     */
	@Override
	public boolean throwBall(int count);
	
}
