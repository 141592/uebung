class X {
  int a = 4;

  int get() {
    return a;
  }
}

class Y extends X {
  static int a = 7;

  int get() {
    return a;
  }

  static void set(int x) {
    a = x;
  }

  static void set(char c) {
    a = 2 * c;
  }
}

class Z extends Y {
  static int b = 3;

  int get() {
    return b + a;
  }

  static int get(X x) {
    return x.a;
  }

  static void set(int i) {
    a = 3 * i;
   }

  static void set(X x, int i) {
    a = i;
  }

  static void test() {
    Z z = new Z();

    System.out.println("Y.a: " + Y.a); // ________
    System.out.println("get(z): " + get(z)); // ________

    Z.set('c' - 'a' - 1); // ASCII Werte: 'c' = 99, 'a' = 97
    System.out.println("get(z): " + get(z)); // ________
    System.out.println("z.get() " + z.get()); // ________

    Y y = z;
    Y.set(2);
    System.out.println("z.get() + 1: "  + (z.get() + 1)); // ________
    Z.set(y, 0);
    System.out.println("y.get() + 4: " + (y.get() + 4)); // ________
  }

  public static void main(String args[]) {
    Z.test();
  }
}
